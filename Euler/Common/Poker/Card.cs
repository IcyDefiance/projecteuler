﻿using System;

namespace Euler.Common.Poker
{
    struct Card
    {
        public int Value;
        public Suit Suit;

        public Card(int value, Suit suit)
        {
            Value = value;
            Suit = suit;
        }

        public Card(string desc)
        {
            if (desc[0] < 'A')
                Value = desc[0] - '0';
            else if (desc[0] == 'T')
                Value = 10;
            else if (desc[0] == 'J')
                Value = 11;
            else if (desc[0] == 'Q')
                Value = 12;
            else if (desc[0] == 'K')
                Value = 13;
            else if (desc[0] == 'A')
                Value = 14;
            else
                throw new ArgumentException("Invalid card number");

            if (desc[1] == 'C')
                Suit = Suit.Clubs;
            else if (desc[1] == 'D')
                Suit = Suit.Diamonds;
            else if (desc[1] == 'H')
                Suit = Suit.Hearts;
            else if (desc[1] == 'S')
                Suit = Suit.Spades;
            else
                throw new ArgumentException("Invalid card suit");
        }
    }

    enum Suit
    {
        Clubs,
        Diamonds,
        Hearts,
        Spades
    }
}
