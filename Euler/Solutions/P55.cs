﻿using Euler.Common;
using System;
using System.Linq;
using System.Numerics;

namespace Euler.Solutions
{
    class P55
    {
        public static void Run()
        {
            int answer = 0;
            for (int i = 0; i < 10000; i++)
            {
                BigInteger n = i;
                bool isLychrel = true;
                for (int j = 0; j < 50 && isLychrel; j++)
                {
                    n += BigInteger.Parse(new string(n.ToString().Reverse().ToArray()));
                    if (Utility.IsPalindrome(n.ToString()))
                        isLychrel = false;
                }

                if (isLychrel)
                    answer++;
            }

            Console.WriteLine(answer);
        }
    }
}
