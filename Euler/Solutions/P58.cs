﻿using Euler.Common;
using System;
using System.Numerics;

namespace Euler.Solutions
{
    class P58
    {
        public static void Run()
        {
            BigInteger i = 3, primeCount = 0, diagCount = 1;
            while (true)
            {
                if (i % 1000 == 1)
                    Console.Write("|");

                BigInteger max = i * i;
                if (max.IsPrime())
                    primeCount++;
                if ((max - i + 1).IsPrime())
                    primeCount++;
                if ((max - i - i + 2).IsPrime())
                    primeCount++;
                if ((max - i - i - i + 3).IsPrime())
                    primeCount++;

                diagCount += 4;

                if (primeCount * 10 < diagCount)
                    break;

                i += 2;
            }

            Console.WriteLine();
            Console.WriteLine(i);
        }
    }
}
