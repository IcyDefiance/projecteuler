﻿using System;
using Euler.Common;
using System.Numerics;

namespace Euler.Solutions
{
    class P53
    {
        public static void Run()
        {
            BigInteger limit = 1000000;

            int total = 0;
            for (BigInteger n = 1; n <= 100; n++)
                for (BigInteger r = 1; r <= n; r++)
                    if (Utility.XtakeY(n, r) > limit)
                        total++;

            Console.WriteLine(total);
        }
    }
}
