﻿using Euler.Common.Poker;
using System;
using System.IO;
using System.Linq;

namespace Euler.Solutions
{
    class P54
    {
        public static void Run()
        {
            Console.WriteLine(File.ReadAllText(@"Resources\p054_poker.txt")                                     //read file
                .Split(new[] { '\n', ' ' }, StringSplitOptions.RemoveEmptyEntries)                              //split by line and spaces
                .Select((card, i) => new { Card = card, Index = i })                                            //map cards to indices
                .GroupBy(keySelector:     cardMap => cardMap.Index / 5,                                         //group into hands
                         elementSelector: cardMap => new Card(cardMap.Card),
                         resultSelector:  (key, handCards) => new { Hand = new Hand(handCards), Index = key })
                .GroupBy(keySelector:     handMap => handMap.Index / 2,                                         //group into players
                         elementSelector: handMap => handMap.Hand,
                         resultSelector:  (key, players) => players)
                .Count(line => line.First() > line.Last()));                                                    //count hands where first player wins
        }
    }
}
