﻿using System;
using System.Linq;
using System.Numerics;

namespace Euler.Solutions
{
    class P56
    {
        public static void Run()
        {
            int maxDigitSum = 0;
            for (BigInteger a = 0; a < 100; a++)
            {
                for (int b = 0; b < 100; b++)
                {
                    int digitSum = BigInteger.Pow(a, b).ToString().Select(x => x - '0').Sum();
                    if (digitSum > maxDigitSum)
                        maxDigitSum = digitSum;
                }
            }

            Console.WriteLine(maxDigitSum);
        }
    }
}
