﻿using System;
using System.Numerics;

namespace Euler.Solutions
{
    class P57
    {
        public static void Run()
        {
            var expansions = 1000;
            var sqrt2 = new Fraction(2, 5);
            var answer = 0;

            for (int i = 0; i < expansions - 2; i++)
            {
                if (i % 25 == 0)
                    Console.Write("|");
                sqrt2 = 1 / (2 + sqrt2);

                var sqrt2real = 1 + sqrt2;
                if (sqrt2real.Numerator.ToString().Length > sqrt2real.Denominator.ToString().Length)
                    answer++;
            }
            sqrt2++;

            Console.WriteLine();
            Console.WriteLine(answer);
        }
    }

    struct Fraction
    {
        public BigInteger Numerator;
        public BigInteger Denominator;

        public Fraction(BigInteger num, BigInteger den)
        {
            Numerator = num;
            Denominator = den;
            Simplify();
        }

        public override string ToString() { return Numerator + "/" + Denominator; }

        public Fraction Reciprocal { get { return new Fraction(Denominator, Numerator); } }

        public static Fraction operator +(Fraction left, Fraction right)
        {
            return new Fraction(
                left.Numerator * right.Denominator + right.Numerator * left.Denominator,
                left.Denominator * right.Denominator
            );
        }

        public static Fraction operator *(Fraction left, Fraction right)
        {
            return new Fraction(
                left.Numerator * right.Numerator,
                left.Denominator * right.Denominator
            );
        }

        public static Fraction operator /(Fraction left, Fraction right)
        {
            return left * right.Reciprocal;
        }

        public static Fraction operator ++(Fraction left)
        {
            return new Fraction(left.Numerator + left.Denominator, left.Denominator);
        }

        public static implicit operator Fraction(long source) { return new Fraction(source, 1); }

        void Simplify()
        {
            var gcd = Gcd(Numerator, Denominator);
            Numerator /= gcd;
            Denominator /= gcd;
        }

        BigInteger Gcd(BigInteger a, BigInteger b)
        {
            return b == 0 ? a : Gcd(b, a % b);
        }
    }
}
