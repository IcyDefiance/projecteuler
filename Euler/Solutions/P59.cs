﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euler.Solutions
{
    class P59
    {
        public static void Run()
        {
            var cipher = File.ReadAllText("Resources/p059_cipher.txt").Split(',').Select(x => (char)int.Parse(x));
            var words = File.ReadAllLines("Resources/words.txt");

            char[] key = new char[3];
            for (key[0] = 'a'; key[0] <= 'z'; key[0]++)
            {
                for (key[1] = 'a'; key[1] <= 'z'; key[1]++)
                {
                    for (key[2] = 'a'; key[2] <= 'z'; key[2]++)
                    {
                        var text = new string(cipher
                            .Select((x, i) => x ^ key[i % 3])
                            .Select(x => (char)x)
                            .ToArray()
                        );

                        var charRank = text.GroupBy(x => x)
                            .OrderByDescending(x => x.Count())
                            .Select(x => new { Letter = x.Key, Frequency = x.Count() / text.Length })
                            .ToList();

                        var symbols = new[] { '.', '&', '(', '-', ',', '+', '*', ')', '\'', '%', '`', '$', '#', '"', '!', '/', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '<', '>', '=', ';', ':', '?' };
                        if (!symbols.Contains(charRank[0].Letter) && !symbols.Contains(charRank[1].Letter) && !symbols.Contains(charRank[2].Letter) && !symbols.Contains(charRank[3].Letter) && text.Contains("The"))
                        {
                            Console.WriteLine(text.Sum(x => x));
                        }
                    }
                }
            }
        }
    }
}
