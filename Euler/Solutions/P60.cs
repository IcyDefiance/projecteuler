﻿using Euler.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euler.Solutions
{
    class P60
    {
        public static void Run()
        {
            var primes = Enumerable.Range(0, 10000).Where(x => x.IsPrime()).ToList();
            int lowsum = int.MaxValue;
            
            for (int a = 0; a < primes.Count; a++)
            {
                if (primes[a] + primes[a + 1] + primes[a + 2] + primes[a + 3] + primes[a + 4] >= lowsum)
                    break;

                Console.Write("|");

                int pa = primes[a];
                int mpa = (int)Math.Pow(10, (int)Math.Log10(pa) + 1);
                for (int b = a + 1; b < primes.Count; b++)
                {
                    int pb = primes[b];
                    int mpb = (int)Math.Pow(10, (int)Math.Log10(pb) + 1);

                    if ((pa * mpb + pb).IsPrime() &&
                        (pb * mpa + pa).IsPrime())
                    {
                        for (int c = b + 1; c < primes.Count; c++)
                        {
                            int pc = primes[c];
                            int mpc = (int)Math.Pow(10, (int)Math.Log10(pc) + 1);

                            if ((pa * mpc + pc).IsPrime() &&
                                (pb * mpc + pc).IsPrime() &&
                                (pc * mpa + pa).IsPrime() &&
                                (pc * mpb + pb).IsPrime())
                            {
                                for (int d = c + 1; d < primes.Count; d++)
                                {
                                    int pd = primes[d];
                                    int mpd = (int)Math.Pow(10, (int)Math.Log10(pd) + 1);

                                    if ((pa * mpd + pd).IsPrime() &&
                                        (pb * mpd + pd).IsPrime() &&
                                        (pc * mpd + pd).IsPrime() &&
                                        (pd * mpa + pa).IsPrime() &&
                                        (pd * mpb + pb).IsPrime() &&
                                        (pd * mpc + pc).IsPrime())
                                    {
                                        for (int e = d + 1; e < primes.Count; e++)
                                        {
                                            int pe = primes[e];
                                            int mpe = (int)Math.Pow(10, (int)Math.Log10(pe) + 1);

                                            if ((pa * mpe + pe).IsPrime() &&
                                                (pb * mpe + pe).IsPrime() &&
                                                (pc * mpe + pe).IsPrime() &&
                                                (pd * mpe + pe).IsPrime() &&
                                                (pe * mpa + pa).IsPrime() &&
                                                (pe * mpb + pb).IsPrime() &&
                                                (pe * mpc + pc).IsPrime() &&
                                                (pe * mpd + pd).IsPrime())
                                            {
                                                int newsum = pa + pb + pc + pd + pe;
                                                if (newsum < lowsum)
                                                {
                                                    lowsum = newsum;

                                                    Console.Clear();
                                                    Console.WriteLine($"{pa} + {pb} + {pc} + {pd} + {pe} = {lowsum}");
                                                    Console.Write(new string('|', a + 1));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Console.WriteLine(lowsum);
        }
    }
}
