﻿using System;
using Euler.Common;

namespace Euler.Solutions
{
    class P52
    {
        public static void Run()
        {
            ulong i = 1;
            for (; !Utility.IsPermutation(i.ToString(), (i * 2).ToString()) ||
                   !Utility.IsPermutation(i.ToString(), (i * 3).ToString()) ||
                   !Utility.IsPermutation(i.ToString(), (i * 4).ToString()) ||
                   !Utility.IsPermutation(i.ToString(), (i * 5).ToString()) ||
                   !Utility.IsPermutation(i.ToString(), (i * 6).ToString())
                 ; i++) { if (i == 0) break; }
            Console.WriteLine("{0} (2x:{1}, 3x:{2}, 4x:{3}, 5x:{4}, 6x:{5})", i, i * 2, i * 3, i * 4, i * 5, i * 6);
        }
    }
}
