﻿using System;
using System.Text;
using Euler.Common;

namespace Euler.Solutions
{
    class P51
    {
        public static void Run()
        {
            ulong i = 0;
            for (; ; i++)
                if (i.IsPrime())
                    if (ReplaceDigits(i) >= 8)
                        break;
        }

        static int maxMaxPrimeCount = 0;
        static int ReplaceDigits(ulong n)
        {
            string nstr = n.ToString();
            int maxPrimeCount = 0;
            int[] repi;

            for (int repnum = 1; repnum < nstr.Length; repnum++)
            {
                repi = new int[repnum];
                for (int i = 0; i < repi.Length; i++)
                    repi[i] = i;
                repi[repi.Length - 1]--;

                do
                {
                    if (repi[repi.Length - 1] < nstr.Length - 1)
                        repi[repi.Length - 1]++;
                    else
                        for (int i = repi.Length - 2; i >= 0; i--)
                            if (repi[i] < repi[i + 1] - 1)
                            {
                                repi[i]++;
                                for (int j = 1; j < repi.Length - i; j++)
                                    repi[i + j] = repi[i] + j;
                                break;
                            }

                    int primeCount = 0;
                    for (int i = 0; i <= 9; i++)
                    {
                        if (repi[0] == 0 && i == 0)
                            continue;
                        StringBuilder nnstr = new StringBuilder(nstr);
                        for (int d = 0; d < repi.Length; d++)
                            nnstr[repi[d]] = (char)(i + '0');
                        if (ulong.Parse(nnstr.ToString()).IsPrime())
                            primeCount++;
                    }

                    if (primeCount > maxPrimeCount)
                    {
                        maxPrimeCount = primeCount;
                        if (maxPrimeCount > maxMaxPrimeCount)
                        {
                            maxMaxPrimeCount = maxPrimeCount;
                            StringBuilder nnstr = new StringBuilder(nstr);
                            for (int i = 0; i < repi.Length; i++)
                                nnstr[repi[i]] = '*';
                            string lowestPrime = nnstr.ToString();
                            for (int i = 0; ; i++)
                                if (ulong.Parse(lowestPrime.Replace('*', (char)(i + '0'))).IsPrime())
                                {
                                    lowestPrime = lowestPrime.Replace('*', (char)(i + '0'));
                                    break;
                                }
                            Console.WriteLine(lowestPrime + " - " + nnstr.ToString() + " - " + maxMaxPrimeCount + " primes");
                        }
                    }
                } while (repi[0] < nstr.Length - repnum);
            }

            return maxPrimeCount;
        }
    }
}
